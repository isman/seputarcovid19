import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:SeputarCovid19/mainscreen.dart';
import 'package:SeputarCovid19/registerscreen.dart';
import 'package:SeputarCovid19/theme.dart';

// void main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await Firebase.initializeApp();
//   runApp(LoginScreen());
// }

var firestoreInstance = FirebaseFirestore.instance;

class LoginScreen extends StatelessWidget {
  static String tag = 'login-page';

  void setar() {
    firestoreInstance.collection("Users").get().then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        print(result.data());
      });
    });
  }

  void searchUsername() {
    firestoreInstance
        .collection("Users")
        .where("username", isEqualTo: "isman")
        .snapshots()
        .listen((result) {
      result.docs.forEach((result) {
        print(result.data());
        print(result.data()['password']);
      });
    });
  }

  TextEditingController tname = TextEditingController();
  TextEditingController tword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // setar();
    // searchUsername();

    final logo = Hero(
      tag: 'SeputarCovid19',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 100.0,
        child: Image.asset('logo.png'),
      ),
    );

    final titleapp = Container(
      child: Center(
        child: Text(
          "Login",
          style: titlePage,
        ),
      ),
    );

    final ujicoba = Container(
      child: Text(
        "Username : admin\nPassword : admin",
        style: titleAppbar,
      ),
    );

    final titlename = Container(
      child: Text(
        "Enter Your Username Here !",
        style: titleAppbar,
      ),
    );

    final uname = TextField(
      style: titleCardLight,
      controller: tname,
      keyboardType: TextInputType.name,
      autofocus: false,
      decoration: borderUsername,
    );

    final titlepass = Container(
      child: Text(
        "Enter Your Password Here !",
        style: titleAppbar,
      ),
    );

    final pword = TextField(
      style: titleCardLight,
      controller: tword,
      keyboardType: TextInputType.visiblePassword,
      autofocus: false,
      decoration: borderPassword,
    );

    final registernow = Container(
      child: Text(
        "Create New Account",
        style: titleAppbar,
      ),
    );

    final loginbtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Container(
        height: 50,
        decoration: submitBtn,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          splashColor: lightPurple,
          color: purple,
          child: Text(
            "Login",
            style: titleCardLight,
          ),
          onPressed: () {
            firestoreInstance
                .collection("Users")
                .where("username", isEqualTo: tname.text)
                .snapshots()
                .listen((result) {
              result.docs.forEach((result) {
                if (result.data()['password'] == tword.text) {
                  // print(result.data()['password']);
                  // print(result.data());
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MainScreen()),
                  );
                }
              });
            });
          },
        ),
      ),
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [purple, lightPurple],
          ),
        ),
        padding: EdgeInsets.all(20.0),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: false,
          body: Center(
            child: SingleChildScrollView(
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  logo,
                  SizedBox(height: 30.0),
                  titleapp,
                  SizedBox(height: 30.0),
                  // ujicoba,
                  // SizedBox(height: 10.0),
                  titlename,
                  SizedBox(height: 10.0),
                  uname,
                  SizedBox(height: 20.0),
                  titlepass,
                  SizedBox(height: 10.0),
                  pword,
                  SizedBox(height: 20.0),
                  loginbtn,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegisterScreen()),
                          );
                        },
                        child: registernow,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // super.dispose();
    tname.dispose();
    tword.dispose();
  }
}
