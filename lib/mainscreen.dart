import 'dart:io';
import 'package:SeputarCovid19/getapi.dart';
import 'package:SeputarCovid19/loginsceen.dart';
import 'package:SeputarCovid19/theme.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

// Future<DataGet> indonesiaData() async {
//   // Alamat api
//   String apiURLPOST = 'https://api.kawalcorona.com/indonesia';

//   var res = await http
//       .get(Uri.encodeFull(apiURLPOST), headers: {"Accept": "application/json"});
//   var resBody = json.decode(res.body);
//   // this.setState(() {
//   //   var resBody = json.decode(res.body);
//   // idconfirmed = resBody['positif'];
//   // idrecovered = resBody['sembuh'];
//   // iddeaths = resBody['meninggal'];
//   // });
//   return DataGet.indonesiaGet(jsonDecode(res.body));
// }

class MainScreen extends StatefulWidget {
  static String tag = 'main-page';
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreen createState() => _MainScreen();
}

class _MainScreen extends State<MainScreen> {
  DataGet globalpositif;
  DataGet globalsembuh;
  DataGet globalmeninggal;
  String idconfirmed;
  String idrecovered;
  String iddeaths;
  String jabarconfirmed;
  String jabarrecovered;
  String jabardeaths;

  @override
  void initState() {
    super.initState();

    // Set State Global Data
    DataGet.globalpositif().then((value) {
      setState(() {
        globalpositif = value;
      });
    });
    DataGet.globalsembuh().then((value) {
      setState(() {
        globalsembuh = value;
      });
    });
    DataGet.globalmeninggal().then((value) {
      setState(() {
        globalmeninggal = value;
      });
    });

    // Set State Jabar Data
    DataGet().jabarConfirmed().then((value) {
      setState(() {
        jabarconfirmed = value;
      });
    });
    DataGet().jabarRecovered().then((value) {
      setState(() {
        jabarrecovered = value;
      });
    });
    DataGet().jabarDeaths().then((value) {
      setState(() {
        jabardeaths = value;
      });
    });

    // Set State Indonesia Data
    DataGet().indonesiaConfirmed().then((value) {
      setState(() {
        idconfirmed = value;
      });
    });
    DataGet().indonesiaRecovered().then((value) {
      setState(() {
        idrecovered = value;
      });
    });
    DataGet().indonesiaDeaths().then((value) {
      setState(() {
        iddeaths = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // print(iddeaths);
    // print(idconfirmed);
    // print(idrecovered);
    // DataGet.jabarConfirmed().then((value) => print("value: $value"));

    var formatter = NumberFormat("###,###,###", "en_US");

    const wa = "https://wa.me/+6283829124631/?text=Seputar Covid-19";
    const survey =
        "https://docs.google.com/forms/d/e/1FAIpQLSdj15oM9zPvi1HjW18ldYmS-otL4PoNgD3yGG110OBjuoBwkg/viewform?usp=sf_link";

    _launchUrl(String url) async {
      var encoded = Uri.encodeFull(url);
      if (await canLaunch(encoded)) {
        await launch(encoded);
      } else {
        throw 'Could not launch $url';
      }
    }

    final callcenter = Container(
      margin: EdgeInsets.all(10),
      width: 140,
      decoration: card,
      child: Material(
        shape: materialcard,
        color: Colors.transparent,
        child: InkWell(
          borderRadius: bordercard,
          splashColor: lightPurple,
          onTap: () => launch("tel:119"),
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Call Center",
                      style: titleCard,
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Nomor Darurat",
                      style: subtitleCard,
                    ),
                    SizedBox(height: 10),
                    Text(
                      "119",
                      style: callcenternumber,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Icon(
                        Icons.phone,
                        color: purple,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    final chatwa = Container(
      margin: EdgeInsets.all(10),
      width: 170,
      decoration: card,
      child: Material(
        shape: materialcard,
        color: Colors.transparent,
        child: InkWell(
          borderRadius: bordercard,
          splashColor: lightPurple,
          onTap: () {
            _launchUrl(wa);
          },
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Hubungi Kami",
                      style: titleCard,
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Seputar COVID-19",
                      style: subtitleCard,
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Chat Sekarang ?",
                      style: walogo,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Image.asset(
                        "img/whatsapp.png",
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    final positif = Container(
      margin: EdgeInsets.all(10),
      width: 300,
      decoration: positifCard,
      child: Material(
        shape: materialcard,
        color: Colors.transparent,
        child: InkWell(
          borderRadius: bordercard,
          splashColor: lightPurple,
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      "Positif",
                      style: titleCardLight,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (jabarconfirmed != null)
                              ? formatter.format(int.parse(jabarconfirmed))
                              : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Jawa Barat",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (idconfirmed != null) ? idconfirmed : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Indonesia",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (globalpositif != null)
                              ? globalpositif.globalconfirmed
                              : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Dunia",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );

    final sembuh = Container(
      margin: EdgeInsets.all(10),
      width: 300,
      decoration: sembuhCard,
      child: Material(
        shape: materialcard,
        color: Colors.transparent,
        child: InkWell(
          borderRadius: bordercard,
          splashColor: lightPurple,
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      "SEMBUH",
                      style: titleCardLight,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (jabarrecovered != null)
                              ? formatter.format(int.parse(jabarrecovered))
                              : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Jawa Barat",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (idrecovered != null) ? idrecovered : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Indonesia",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (globalsembuh != null)
                              ? globalsembuh.globalrecovered
                              : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Dunia",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );

    final meninggal = Container(
      margin: EdgeInsets.all(10),
      width: 300,
      decoration: meninggalCard,
      child: Material(
        shape: materialcard,
        color: Colors.transparent,
        child: InkWell(
          borderRadius: bordercard,
          splashColor: lightPurple,
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      "MENINGGAL",
                      style: titleCardLight,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (jabardeaths != null)
                              ? formatter.format(int.parse(jabardeaths))
                              : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Jawa Barat",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (iddeaths != null) ? iddeaths : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Indonesia",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: <Widget>[
                        Text(
                          (globalmeninggal != null)
                              ? globalmeninggal.globaldeaths
                              : "Error",
                          style: titleCardLight,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Dunia",
                          style: titleCardLight,
                        ),
                      ],
                    ),
                    Spacer(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );

    final refreshbtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Container(
        height: 50,
        decoration: submitBtn,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          splashColor: lightPurple,
          color: purple,
          child: Icon(
            Icons.refresh,
            color: white,
          ),
          onPressed: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MainScreen()),
            );
          },
        ),
      ),
    );

    final surveybtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Container(
        height: 50,
        decoration: submitBtn,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          splashColor: lightPurple,
          color: white,
          child: Text(
            "Isi Survey sekarang ?",
            style: titleCard,
          ),
          onPressed: () {
            _launchUrl(survey);
          },
        ),
      ),
    );

    final body = Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
          image: AssetImage(
            "img/background.png",
          ),
          fit: BoxFit.cover,
        ),
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                callcenter,
                chatwa,
              ],
            ),
            positif,
            sembuh,
            meninggal,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Spacer(),
                refreshbtn,
                Spacer(),
                surveybtn,
                Spacer(),
              ],
            ),
          ],
        ),
      ),
    );

    return WillPopScope(
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Warning'),
          content: Text('Do you really want to exit'),
          actions: [
            FlatButton(
              child: Text(
                'Yes',
                style: choicesyes,
              ),
              onPressed: () {
                exit(0);
              },
            ),
            RaisedButton(
              splashColor: lightPurple,
              color: meninggalTerang,
              child: Text(
                'No',
                style: choicesno,
              ),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
      child: Scaffold(
        appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: neon,
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3),
              )
            ]),
            child: AppBar(
              backgroundColor: purple,
              automaticallyImplyLeading: false,
              centerTitle: true,
              title: Row(
                children: <Widget>[
                  Image.asset("logo.png", fit: BoxFit.cover, height: 40.0),
                  Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Text(
                      "Informasi Seputar COVID-19",
                      style: titleAppbar,
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.logout),
                  color: white,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                    );
                  },
                ),
              ],
            ),
          ),
          preferredSize: Size.fromHeight(kToolbarHeight),
        ),
        body: body,
      ),
    );
  }
}
