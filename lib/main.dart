import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:SeputarCovid19/loginsceen.dart';
import 'package:SeputarCovid19/mainscreen.dart';
import 'package:SeputarCovid19/registerscreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginScreen.tag: (context) => LoginScreen(),
    RegisterScreen.tag: (context) => RegisterScreen(),
    MainScreen.tag: (context) => MainScreen(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tugas UAS Mobile Programming',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Poppins'),
      home: LoginScreen(),
      routes: routes,
    );
  }
}
