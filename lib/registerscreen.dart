import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:SeputarCovid19/loginsceen.dart';
import 'package:SeputarCovid19/theme.dart';

var firestoreInstance = FirebaseFirestore.instance;

class RegisterScreen extends StatelessWidget {
  static String tag = 'register-page';

  TextEditingController tname = TextEditingController();
  TextEditingController tword = TextEditingController();
  TextEditingController tconfirmword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'SeputarCovid19',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 100.0,
        child: Image.asset('logo.png'),
      ),
    );

    final titleapp = Container(
      child: Center(
        child: Text(
          "Registration",
          style: titlePage,
        ),
      ),
    );

    final titlename = Container(
      child: Text(
        "Enter Your Username Here !",
        style: titleAppbar,
      ),
    );

    final uname = TextField(
      style: titleCardLight,
      controller: tname,
      keyboardType: TextInputType.name,
      autofocus: false,
      decoration: borderUsername,
    );

    final titlepass = Container(
      child: Text(
        "Enter Your Password Here !",
        style: titleAppbar,
      ),
    );

    final pword = TextField(
      style: titleCardLight,
      controller: tword,
      keyboardType: TextInputType.visiblePassword,
      autofocus: false,
      decoration: borderPassword,
    );

    final titleconfirmpass = Container(
      child: Text(
        "Enter Your Confirm Password Here !",
        style: titleAppbar,
      ),
    );

    final confirmpword = TextField(
      style: titleCardLight,
      controller: tconfirmword,
      keyboardType: TextInputType.visiblePassword,
      autofocus: false,
      decoration: borderConfirmPassword,
    );

    final loginnow = Container(
      child: Text(
        "Already have an account",
        style: titleAppbar,
      ),
    );

    final registerbtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Container(
        height: 50,
        decoration: submitBtn,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          splashColor: lightPurple,
          color: purple,
          child: Text(
            "Register",
            style: titleCardLight,
          ),
          onPressed: () {
            if (tname.text != '' &&
                tword.text != '' &&
                tconfirmword.text != '' &&
                tword.text == tconfirmword.text) {
              firestoreInstance.collection("Users").add({
                "username": tname.text,
                "password": tword.text,
              }).then((value) {
                // print(value.id);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              });
            } else {
              print(tname.text);
              // print(tword.text);
            }
          },
        ),
      ),
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [purple, lightPurple],
          ),
        ),
        padding: EdgeInsets.all(20.0),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: false,
          body: Center(
            child: SingleChildScrollView(
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  logo,
                  SizedBox(height: 30.0),
                  titleapp,
                  SizedBox(height: 10.0),
                  titlename,
                  SizedBox(height: 10.0),
                  uname,
                  SizedBox(height: 20.0),
                  titlepass,
                  SizedBox(height: 10.0),
                  pword,
                  SizedBox(height: 20.0),
                  titleconfirmpass,
                  SizedBox(height: 10.0),
                  confirmpword,
                  SizedBox(height: 20.0),
                  registerbtn,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen()),
                          );
                        },
                        child: loginnow,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // super.dispose();
    tname.dispose();
    tword.dispose();
    tconfirmword.dispose();
  }
}
