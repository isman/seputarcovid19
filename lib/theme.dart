import 'package:flutter/material.dart';

Color purple = Color(0xff74309B);
Color white = Color(0xffFFFFFF);
Color lightPurple = Color(0xffb388ff);
Color black = Color(0xff262327);
Color positif = Color(0xffFE8700);
Color positifTerang = Color(0xffFEDC00);
Color sembuh = Color(0xff5AAA4E);
Color sembuhTerang = Color(0xff3FF000);
Color meninggal = Color(0xffFE002A);
Color meninggalTerang = Color(0xffFF0631);
Color whatsapp = Color(0xff20a83e);
Color neon = Color(0xffcf40ff);

TextStyle titlePage = TextStyle(
  color: white,
  fontWeight: FontWeight.w500,
  fontSize: 30,
);

TextStyle hintText = TextStyle(
  color: white,
  fontWeight: FontWeight.w500,
  fontSize: 15,
);

TextStyle titleAppbar = TextStyle(
  color: white,
  fontWeight: FontWeight.w500,
  fontSize: 14,
);

TextStyle titleCard = TextStyle(
  color: black,
  fontWeight: FontWeight.bold,
  fontFamily: "Poppins",
  fontSize: 15,
);

TextStyle titleCardLight = TextStyle(
  color: white,
  fontWeight: FontWeight.bold,
  fontSize: 15,
  fontFamily: "Poppins",
);

TextStyle subtitleCard = TextStyle(
  color: black,
  fontSize: 10,
  fontFamily: "Poppins",
);

TextStyle callcenternumber = TextStyle(
  color: purple,
  fontWeight: FontWeight.bold,
  fontSize: 15,
);

TextStyle walogo = TextStyle(
  color: whatsapp,
  fontWeight: FontWeight.bold,
  fontSize: 15,
);

TextStyle choicesno = TextStyle(
  color: white,
  fontWeight: FontWeight.bold,
  fontSize: 15,
);

TextStyle choicesyes = TextStyle(
  color: meninggal,
  fontWeight: FontWeight.bold,
  fontSize: 15,
);

BoxDecoration card = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.all(Radius.circular(10)),
  boxShadow: [
    BoxShadow(
      color: neon,
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3),
    ),
  ],
);

BoxDecoration positifCard = BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(10)),
  boxShadow: [
    BoxShadow(
      color: neon,
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3),
    ),
  ],
  gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        positif,
        positifTerang,
      ]),
);

BoxDecoration sembuhCard = BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(10)),
  boxShadow: [
    BoxShadow(
      color: neon,
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3),
    ),
  ],
  gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        sembuh,
        sembuhTerang,
      ]),
);

BoxDecoration meninggalCard = BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(10)),
  boxShadow: [
    BoxShadow(
      color: neon,
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3),
    ),
  ],
  gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        meninggal,
        meninggalTerang,
      ]),
);

BoxDecoration roundcard = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.all(Radius.circular(10)),
);

BoxDecoration submitBtn = BoxDecoration(
  color: neon,
  borderRadius: BorderRadius.circular(20),
  boxShadow: [
    BoxShadow(
      color: neon,
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(2, 4),
    ),
  ],
);

BorderRadius bordercard = BorderRadius.all(Radius.circular(10));

RoundedRectangleBorder materialcard =
    RoundedRectangleBorder(borderRadius: BorderRadius.circular(10));

InputDecoration borderUsername = InputDecoration(
  hintText: 'Your Username',
  hintStyle: hintText,
  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(32.0),
    borderSide: BorderSide(
      color: neon,
      width: 1,
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(32.0),
    borderSide: BorderSide(
      color: white,
      width: 1,
    ),
  ),
);

InputDecoration borderPassword = InputDecoration(
  hintText: 'Your Password',
  hintStyle: hintText,
  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(32.0),
    borderSide: BorderSide(
      color: neon,
      width: 1,
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(32.0),
    borderSide: BorderSide(
      color: white,
      width: 1,
    ),
  ),
);

InputDecoration borderConfirmPassword = InputDecoration(
  hintText: 'Your Confirm Password',
  hintStyle: hintText,
  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(32.0),
    borderSide: BorderSide(
      color: neon,
      width: 1,
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.circular(32.0),
    borderSide: BorderSide(
      color: white,
      width: 1,
    ),
  ),
);
