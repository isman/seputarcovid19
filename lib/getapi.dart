import 'package:http/http.dart' as http;
import 'dart:convert';

class DataGet {
  String globalconfirmed,
      globalrecovered,
      globaldeaths,
      idconfirmed,
      idrecovered,
      iddeaths,
      jabarconfirmed,
      jabarrecovered,
      jabardeaths;
  List<DataGet> jb;

  DataGet(
      {this.globalconfirmed,
      this.globalrecovered,
      this.globaldeaths,
      this.idconfirmed,
      this.idrecovered,
      this.iddeaths,
      this.jabarconfirmed,
      this.jabarrecovered,
      this.jabardeaths,
      this.jb});

  static Future<DataGet> globalpositif() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/positif';
    // Get data masih mentah
    var apiResult = await http.get(apiURLPOST);
    // Data dijadikan json
    var jsonObject = json.decode(apiResult.body);
    // Mapping data
    var globalData = (jsonObject as Map<String, dynamic>);
    return DataGet.positifGet(globalData);
  }

  factory DataGet.positifGet(Map<String, dynamic> object) {
    return DataGet(
      globalconfirmed: object['value'].toString(),
      // string
      // nama_var: object['data'],
    );
  }

  static Future<DataGet> globalsembuh() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/sembuh';
    // Get data masih mentah
    var apiResult = await http.get(apiURLPOST);
    // Data dijadikan json
    var jsonObject = json.decode(apiResult.body);
    // Mapping data
    var globalData = (jsonObject as Map<String, dynamic>);
    return DataGet.sembuhGet(globalData);
  }

  factory DataGet.sembuhGet(Map<String, dynamic> object) {
    return DataGet(
      globalrecovered: object['value'].toString(),
    );
  }

  static Future<DataGet> globalmeninggal() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/meninggal';
    // Get data masih mentah
    var apiResult = await http.get(apiURLPOST);
    // Data dijadikan json
    var jsonObject = json.decode(apiResult.body);
    // Mapping data
    var globalData = (jsonObject as Map<String, dynamic>);
    return DataGet.meningggalGet(globalData);
  }

  factory DataGet.meningggalGet(Map<String, dynamic> object) {
    return DataGet(
      globaldeaths: object['value'].toString(),
    );
  }

  // Future<String> indonesiaAPI() async {
  //   // Alamat api
  //   String apiURLPOST = 'https://api.kawalcorona.com/indonesia';
  //   final response = await http.get(apiURLPOST);
  //   List list = json.decode(response.body);
  //   idconfirmed = list[0]['positif'];
  //   idrecovered = list[0]['sembuh'];
  //   iddeaths = list[0]['meninggal'];
  //   // print(idconfirmed);
  //   // return idconfirm;
  // }

  // @override
  // String toString() {
  //   return 'idconfirmed: $idconfirmed, idrecovered: $idrecovered, iddeaths: $iddeaths';
  // }

  // Future<List<DataGet>> getIndonesia() async {
  //   // Alamat api
  //   String apiURLPOST = 'https://api.kawalcorona.com/indonesia';
  //   final response = await http.get(apiURLPOST);
  //   var data = json.decode(response.body);
  //   if (response.statusCode == 200) {
  //     // print(data);
  //     return indonesiaFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // factory DataGet.indonesiaGet(Map<String, dynamic> object) {
  //   return DataGet(
  //       idconfirmed: object['positif'],
  //       idrecovered: object['sembuh'],
  //       iddeaths: object['meninggal']);
  // }

  // List<DataGet> indonesiaFromJson(String jsonData) {
  //   final data = json.decode(jsonData);
  //   return List<DataGet>.from(data.map((item) => DataGet.indonesiaGet(item)));
  // }

  Future<String> indonesiaConfirmed() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/indonesia';
    final response = await http.get(apiURLPOST);
    List list = json.decode(response.body);
    idconfirmed = list[0]['positif'];
    // print(idconfirmed);
    return idconfirmed;
  }

  Future<String> indonesiaRecovered() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/indonesia';
    final response = await http.get(apiURLPOST);
    List list = json.decode(response.body);
    idrecovered = list[0]['sembuh'];
    return idrecovered;
  }

  Future<String> indonesiaDeaths() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/indonesia';
    final response = await http.get(apiURLPOST);
    List list = json.decode(response.body);
    iddeaths = list[0]['meninggal'];
    return iddeaths;
  }

  factory DataGet.JabarGet(Map<String, dynamic> object) {
    return DataGet(
        jabarconfirmed: object['Kasus_Posi'],
        jabarrecovered: object['Kasus_Semb'],
        jabardeaths: object['Kasus_Meni']);
  }

  Future<String> jabarConfirmed() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/indonesia/provinsi';
    final response = await http.get(apiURLPOST);
    List<dynamic> values = new List<dynamic>();
    values = json.decode(response.body);
    if (values.length > 0) {
      if (values[1] != null) {
        Map<String, dynamic> map = values[1]['attributes'];
        jabarconfirmed = map['Kasus_Posi'].toString();
        // print(map);
        // print(jbconfirm);
      }
    }
    return jabarconfirmed;
  }

  Future<String> jabarRecovered() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/indonesia/provinsi';
    final response = await http.get(apiURLPOST);
    List<dynamic> values = new List<dynamic>();
    values = json.decode(response.body);
    if (values.length > 0) {
      if (values[1] != null) {
        Map<String, dynamic> map = values[1]['attributes'];
        jabarrecovered = map['Kasus_Semb'].toString();
        // print(map);
      }
    }
    return jabarrecovered;
  }

  Future<String> jabarDeaths() async {
    // Alamat api
    String apiURLPOST = 'https://api.kawalcorona.com/indonesia/provinsi';
    final response = await http.get(apiURLPOST);
    List<dynamic> values = new List<dynamic>();
    values = json.decode(response.body);
    if (values.length > 0) {
      if (values[1] != null) {
        Map<String, dynamic> map = values[1]['attributes'];
        jabardeaths = map['Kasus_Meni'].toString();
        // print(map);
      }
    }
    return jabardeaths;
  }
}
