<!--
    TIF RM 18 CNS
    Ade Isman Aji               18 111 179
    Moch Dzulvie Aldyansyah     18 111 211
    Sindu Prakasa Lesmana       18 111 297
    Zeni Malik Abdullah         18 111 237
-->
[![Contributors][contributors-shield]][contributors-url]
[![Matkul][matkul-shield]][matkul-url]
[![Flutter][flutter-shield]][flutter-url]
[![Icon][icon-shield]][icon-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="logo.png">
    <img src="logo.png" alt="Logo" width="180">
  </a>

  <h3 align="center">Aplikasi SeputarCovid19</h3>

  <p align="center">
    Di situasi pandemi COVID-19, informasi terbaru tentang perkembangan COVID-19 sangat dibutuhkan.
    <br />
    Dunia, Indonesia, dan Jawa Barat.
    <br />
    Tetap jaga jarak :smiley:
    <br />
    <br />
    <a href="https://gitlab.com/isman/seputarcovid19/-/raw/master/SeputarCovid19.apk">Seputar COVID-19</a>
    <br />
    <br />
    <a href="https://gitlab.com/isman/seputarcovid19/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/isman/seputarcovid19/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#built-with">Built With</a></li>
    <li><a href="#contributors">Contributors</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

Project ini dibuat sebagai tugas UAS Mata Kuliah Mobile Programming 2

<br />
<img src="https://gitlab.com/isman/seputarcovid19/-/raw/master/Screen%20Register.JPG" alt="drawing" width="200"/>
<img src="https://gitlab.com/isman/seputarcovid19/-/raw/master/Screen%20Login.JPG" alt="drawing" width="200"/>
<img src="https://gitlab.com/isman/seputarcovid19/-/raw/master/Screen%20Main.JPG" alt="drawing" width="200"/>
<br />

## Built With

* [Flutter](https://flutter.dev/docs/development)
* [Material Icons](https://material.io/resources/icons/?style=baseline)
* [API](https://kawalcorona.com/api/)

<!-- Contributors -->
## Contributors

TIF RM 18 CNS :
1. [Ade Isman Aji](https://gitlab.com/isman)
2. [Moch Dzulvie Aldyansyah](https://gitlab.com/mochdzulvie)
3. [Sindu Prakasa Lesmana](https://gitlab.com/sinduprakasa04)
4. [Zeni Malik Abdulah](https://gitlab.com/zenimalikabdulah)

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- CONTACT -->
## Contact

Project Link: [SeputarCovid19](https://github.com/isman/seputarcovid19)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/static/v1?label=Contributors&message=4&color=green
[contributors-url]: https://gitlab.com/isman/seputarcovid19/-/graphs/master
[matkul-shield]: https://img.shields.io/static/v1?label=Mata%20Kuliah&message=Mobile%20Programming%202&color=white
[matkul-url]: https://gitlab.com/isman/seputarcovid19
[flutter-shield]: https://img.shields.io/static/v1?label=Build&message=Flutter&color=blue
[flutter-url]: https://gitlab.com/isman/seputarcovid19
[icon-shield]: https://img.shields.io/static/v1?label=Icons&message=Material%20Icon&color=grey
[icon-url]: https://gitlab.com/isman/seputarcovid19
